import re

from common import read_lines_in_file_as_string_array


def extract_rule(line):
    container = re.search('^([a-z]+ [a-z]+)', line).group(0)
    containedBagsRaw = re.findall('[0-9]+ [a-z]+ [a-z]+', line)
    containedBags = [[int(containedBag.split(" ")[0]), containedBag.split(" ")[1] + " " + containedBag.split(" ")[2]]
                     for
                     containedBag in containedBagsRaw]
    return [container, containedBags]


def get_rules(input):
    return list(map(extract_rule, input))


def contains(bagType, val, rules):
    rule = next(rule for rule in rules if rule[0] == bagType)
    if rule == None:
        return False
    containedBagTypes = [x[1] for x in rule[1]]
    if val in containedBagTypes:
        return True
    return len(list(filter(lambda bType: contains(bType, val, rules), containedBagTypes))) != 0


def contained_by(rules, val):
    bagTypes = [x[0] for x in rules]
    return list(filter(lambda bType: contains(bType, val, rules), bagTypes))


def count_bags_contained(rules, bagType):
    rule = next(rule for rule in rules if rule[0] == bagType)
    containedBags = rule[1]
    if len(containedBags) == 0:
        return 0
    count = 0
    for bag in containedBags:
        count += bag[0] * (1 + count_bags_contained(rules, bag[1]))
    return count


def part_1(inp):
    rules = get_rules(inp)
    return len(contained_by(rules, "shiny gold"))


def part_2(inp):
    rules = get_rules(inp)
    return count_bags_contained(rules, "shiny gold")


if __name__ == '__main__':
    input_as_string_array = read_lines_in_file_as_string_array()
    print(part_1(input_as_string_array))
    print(part_2(input_as_string_array))
