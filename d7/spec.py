import unittest

from common import read_lines_in_file_as_string_array
from d7 import get_rules, contained_by, count_bags_contained, get_rules

"light red bags contain 1 bright white bag, 2 muted yellow bags."


class TestDay(unittest.TestCase):

    def test_get_rules_2(self):
        input = read_lines_in_file_as_string_array("test_input.txt")
        self.assertEqual(get_rules(input)[0], ["light red", [[1, "bright white"], [2, "muted yellow"]]])

    def test_contained_by(self):
        input = read_lines_in_file_as_string_array("test_input.txt")
        rules = get_rules(input)
        res = contained_by(rules, "shiny gold")
        self.assertEqual(len(res), 4)

    def test_count(self):
        input = read_lines_in_file_as_string_array("test_input.txt")
        rules = get_rules(input)
        res = count_bags_contained(rules, "shiny gold")
        self.assertEqual(res, 32)

    def test_count_2(self):
        input = read_lines_in_file_as_string_array("test_input2.txt")
        rules = get_rules(input)
        res = count_bags_contained(rules, "shiny gold")
        self.assertEqual(res, 126)


if __name__ == '__main__':
    unittest.main()
