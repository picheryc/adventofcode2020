default_path = "input.txt"


def read_lines_in_file_as_int(path=default_path):
    inp = open(path).readlines()
    return list(map(lambda x: int(x[0:-1]), inp))


def read_lines_in_file_as_string_array(path=default_path):
    inp = open(path).readlines()
    return list(map(lambda x: x[0:-1], inp))


def read_first_line_in_file_as_text(path=default_path):
    inp = open(path).readlines()
    return inp[0][:-1]
