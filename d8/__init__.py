from common import read_lines_in_file_as_string_array


def apply_instructions(instructions, index_to_change):
    indexes = []
    index = 0
    accumulator = 0
    last_index = len(instructions) - 1
    while index not in indexes:
        indexes.append(index)
        accumulator, index = instructions[index][0 if index != index_to_change else 1](accumulator, index)
        if index == last_index:
            return True, accumulator
    return False, accumulator


def part_1(instructions):
    indexes = []
    index = 0
    accumulator = 0
    while index not in indexes:
        indexes.append(index)
        accumulator, index = instructions[index][0](accumulator, index)
    return accumulator


def part_2(instructions, to_reverse):
    for i in range(len(instructions)):
        if to_reverse[i]:
            isEnding, accumulator = apply_instructions(instructions, i)
            if isEnding:
                return accumulator
    return "toto"


def nop(accumulator, index):
    return accumulator, index + 1


def acc(value):
    return lambda accumulator, index: (accumulator + value, index + 1)


def jmp(value):
    return lambda accumulator, index: (accumulator, index + value)


def build_instruction(type, value):
    if type == "nop":
        return (nop, jmp(value))
    if type == "acc":
        return (acc(value), acc(value))
    if type == "jmp":
        return (jmp(value), nop)


if __name__ == '__main__':
    input_as_string_array = read_lines_in_file_as_string_array()
    instructions = [build_instruction(x.split(" ")[0], int(x.split(" ")[1])) for x in input_as_string_array]
    to_reverse = [x.split(" ")[0] in ["nop", "jmp"] for x in input_as_string_array]
    print(part_1(instructions))
    print(part_2(instructions, to_reverse))
