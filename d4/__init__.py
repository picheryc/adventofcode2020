import re

from common import read_lines_in_file_as_string_array


def is_valid_field(key, value):
    if key == "byr":
        return int(value) in range(1920, 2003)
    if key == "iyr":
        return int(value) in range(2010, 2021)
    if key == "eyr":
        return int(value) in range(2020, 2031)
    if key == "hgt":
        if "in" in value:
            return int(value.split("in")[0]) in range(59, 77)
        if "cm" in value:
            return int(value.split("cm")[0]) in range(150, 194)
        return False
    if key == "hcl":
        if len(value) != 7:
            return False
        return re.search('#[a-f0-9]{6}', value) is not None
    if key == "ecl":
        return value in ["amb", "blu", "brn", "gry", "grn", "hzl", "oth"]
    if key == "pid":
        return len(value) == 9 and re.search('[0-9]{9}', value) is not None
    if key == "cid":
        return True


def separate_passports(inp):
    passports = []
    current_passport = []
    for line in inp:
        if line == "":
            passports.append(" ".join(current_passport).split(" "))
            current_passport = []
        else:
            current_passport.append(line)
    passports.append(" ".join(current_passport).split(" "))
    return passports


def is_valid_part_1(passport):
    if len(passport) == 8:
        return True
    if len(passport) == 7:
        return sum([key[0:3] == "cid" for key in passport]) == 0
    return False


def is_valid_part_2(passport):
    if not is_valid_part_1(passport):
        return False
    return sum([is_valid_field(field.split(":")[0], field.split(":")[1]) for field in passport]) == len(passport)


def part_1(inp):
    passports = separate_passports(inp)
    return len(list(filter(is_valid_part_1, passports)))


def part_2(inp):
    passports = separate_passports(inp)
    return len(list(filter(is_valid_part_2, passports)))


if __name__ == '__main__':
    input_as_string_array = read_lines_in_file_as_string_array()
    print(part_1(input_as_string_array))
    print(part_2(input_as_string_array))
