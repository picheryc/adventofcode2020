import unittest

from common import read_lines_in_file_as_string_array
from d4 import separate_passports, is_valid_part_1, is_valid_field, part_2

test_input = read_lines_in_file_as_string_array("test_input.txt")


class TestDay(unittest.TestCase):

    def test_separate_passports(self):
        inp = read_lines_in_file_as_string_array("test_input.txt")
        self.assertEqual(len(separate_passports(inp)), 4)

    def test_filter_passport_is_valid_part_1(self):
        passports = separate_passports(read_lines_in_file_as_string_array("test_input.txt"))
        self.assertEqual(is_valid_part_1(passports[0]), True)
        self.assertEqual(is_valid_part_1(passports[1]), False)
        self.assertEqual(is_valid_part_1(passports[2]), True)
        self.assertEqual(is_valid_part_1(passports[3]), False)

    def test_filter_passport_byr_validity(self):
        self.assertEqual(is_valid_field("byr", "1919"), False)
        self.assertEqual(is_valid_field("byr", "1920"), True)
        self.assertEqual(is_valid_field("byr", "2002"), True)
        self.assertEqual(is_valid_field("byr", "2003"), False)

    def test_filter_passport_hgt_validity(self):
        self.assertEqual(is_valid_field("hgt", "60in"), True)
        self.assertEqual(is_valid_field("hgt", "190cm"), True)
        self.assertEqual(is_valid_field("hgt", "190in"), False)
        self.assertEqual(is_valid_field("hgt", "190"), False)

    def test_filter_passport_hcl_validity(self):
        self.assertEqual(is_valid_field("hcl", "#123abc"), True)
        self.assertEqual(is_valid_field("hcl", "#5e832a"), True)
        self.assertEqual(is_valid_field("hcl", "#123abz"), False)
        self.assertEqual(is_valid_field("hcl", "123abc"), False)

    def test_filter_passport_ecl_validity(self):
        self.assertEqual(is_valid_field("ecl", "brn"), True)
        self.assertEqual(is_valid_field("ecl", "wtf"), False)

    def test_filter_passport_pid_validity(self):
        self.assertEqual(is_valid_field("pid", "000000001"), True)
        self.assertEqual(is_valid_field("pid", "0123456789"), False)
        self.assertEqual(is_valid_field("pid", "01234567#"), False)

    def test_part_2(self):
        passports = separate_passports(read_lines_in_file_as_string_array("valid_inputs.txt"))
        self.assertEqual(part_2(read_lines_in_file_as_string_array("valid_inputs.txt")), len(passports))


if __name__ == '__main__':
    unittest.main()
