import numpy as np

from common import read_lines_in_file_as_string_array


def get_adjacent_seats(i, j, occupation):
    adjacent = []
    for k in range(-1, 2):
        for l in range(-1, 2):
            if 0 <= i + k < len(occupation):
                if 0 <= j + l < len(occupation[0]):
                    if not (k == 0 and l == 0):
                        adjacent.append(occupation[i + k][j + l])
    return adjacent


def get_visible_in_dir(i, j, occupation, length, width, dir_i, dir_j):
    visible = "."
    l = i + dir_i
    w = j + dir_j
    while 0 <= l < length and 0 <= w < width:
        if not occupation[l * width + w] == ".":
            return occupation[l * width + w]
        l += dir_i
        w += dir_j
    return visible


def get_visible_seats(i, j, occupation, lenght, width):
    visible = ""
    visible += get_visible_in_dir(i, j, occupation, lenght, width, 0, 1)
    visible += get_visible_in_dir(i, j, occupation, lenght, width, 0, -1)
    visible += get_visible_in_dir(i, j, occupation, lenght, width, 1, 0)
    visible += get_visible_in_dir(i, j, occupation, lenght, width, -1, 0)
    visible += get_visible_in_dir(i, j, occupation, lenght, width, 1, 1)
    visible += get_visible_in_dir(i, j, occupation, lenght, width, 1, -1)
    visible += get_visible_in_dir(i, j, occupation, lenght, width, -1, 1)
    visible += get_visible_in_dir(i, j, occupation, lenght, width, -1, -1)
    return visible


def allocate(occupation):
    new_allocation = [[] for x in range(len(occupation))]
    for i in range(len(occupation)):
        for j in range(len(occupation[0])):
            new_allocation[i].append(occupation[i][j])
            if occupation[i][j] == "L":
                num_occupied = len(list(filter(lambda x: x == '#', get_adjacent_seats(i, j, occupation))))
                if num_occupied == 0:
                    new_allocation[i][j] = "#"
            elif occupation[i][j] == "#":
                num_occupied = len(list(filter(lambda x: x == '#', get_adjacent_seats(i, j, occupation))))
                if num_occupied > 3:
                    new_allocation[i][j] = "L"
    return new_allocation


def allocate2(occupation, lenght, width):
    new_allocation = ""
    for i in range(lenght):
        for j in range(width):
            if occupation[i * width + j] == "L":
                num_occupied = get_visible_seats(i, j, occupation, lenght, width).count("#")
                if num_occupied == 0:
                    new_allocation += "#"
                else:
                    new_allocation += "L"
            elif occupation[i * width + j] == "#":
                num_occupied = get_visible_seats(i, j, occupation, lenght, width).count("#")
                if num_occupied > 4:
                    new_allocation += "L"
                else:
                    new_allocation += "#"
            else:
                new_allocation += "."
    return new_allocation


def part_1(inp):
    occupation = inp
    new_occupation = allocate(occupation)
    while not np.array_equal(new_occupation, occupation):
        occupation = new_occupation
        new_occupation = allocate(occupation)
    sum = 0
    for i in range(len(new_occupation)):
        for j in range(len(new_occupation[0])):
            if new_occupation[i][j] == "#":
                sum += 1
    return sum


def part_2(input, lenght, width):
    occupation = input
    new_occupation = allocate2(occupation, lenght, width)
    while not new_occupation == occupation:
        occupation = new_occupation
        new_occupation = allocate2(occupation, lenght, width)
    return occupation.count("#")


if __name__ == '__main__':
    inp = read_lines_in_file_as_string_array()
    input = "".join(inp)
    lenght = len(inp)
    width = len(inp[0])
    print(part_1(inp))
    print(part_2(input, lenght, width))
