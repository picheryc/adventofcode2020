| Poly | Lien | Langage(s) |
|:------------:|:-----------------------------------------------------------------:|:-------:|
| SRO | [GitHub/sroccaserra](https://github.com/sroccaserra/aoc2020/) | Haskell |
| AUMA | [GitHub/AurelienMassiot](https://github.com/AurelienMassiot/advent_of_code_2020) | Python |
| MOMA | [GitHub/MariaMokbel](https://github.com/MariaMokbel/advent-of-code-2020/) | Python |
| YASC | [GitHub/YannickSchini](https://github.com/YannickSchini/advent_of_code) | Python |
| SILE | [GitHub/silefort](https://github.com/silefort/AdventOfCode) | Python |
| ASA | [GitHub/Akhilian](https://github.com/Akhilian/Advent-of-code-2020) | Python |
| GUIL | [GitHub/jfguilmard](https://github.com/jfguilmard/adventofcode2020) | Python |
| SOF | [GitHub/sofienealouini](https://github.com/sofienealouini/advent-of-code-2020) | Python |
| FRE | [GitHub/rfrenoy](https://github.com/rfrenoy/advent_of_code2020) | Python |
| BEOU | [GitHub/ourmel](https://github.com/ourmel/aventCode2020) | Python |
| VEN | [GitHub/victorenaud](https://github.com/victorenaud/advent-of-code-2020) | Kotlin |
| ROFA | [GitHub/FabienRoussel]( https://github.com/FabienRoussel/Advent-of-Code) | Scala |
| PPR | [GitHub/pprados](https://github.com/pprados) | ? |
| PBB | [GitHub/pierrelandry](https://github.com/pierrelandry) | ? |
| CHRY | [GitLab/picheryc](https://gitlab.com/picheryc/adventofcode2020/) | Python 3 |
