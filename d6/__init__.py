from functools import reduce

from common import read_lines_in_file_as_string_array


def union(result, line):
    if len(result) == 0:
        result.append("")
    if line == "":
        result.append("")
    else:
        result[-1] = set(result[-1]).union(set(line))
    return result


def intersection(result, line):
    if len(result) == 0:
        result.append("abcdefghijklmnopqrstuvwxyz")
    if line == "":
        result.append("abcdefghijklmnopqrstuvwxyz")
    else:
        result[-1] = set(result[-1]).intersection(set(line))
    return result


def part_1(inp):
    return sum([len(x) for x in reduce(union, inp, [])])


def part_2(inp):
    return sum([len(x) for x in reduce(intersection, inp, [])])


if __name__ == '__main__':
    input_as_string_array = read_lines_in_file_as_string_array()
    print(part_1(input_as_string_array))
    print(part_2(input_as_string_array))
