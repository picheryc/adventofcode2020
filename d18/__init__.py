from math import prod

from common import read_lines_in_file_as_string_array


def evaluate(parts):
    new_parts = []
    i = 0
    while i < len(parts):
        opened_parenthesis_count = 0
        closed_parenthesis_count = 0
        if parts[i].startswith("("):
            opened_parenthesis_count += parts[i].count("(")
            k = i + 1
            while closed_parenthesis_count != opened_parenthesis_count:
                k += 1
                opened_parenthesis_count += parts[k].count("(")
                closed_parenthesis_count += parts[k].count(")")
            partI = parts[i][1:]
            partK = parts[k][:-1]
            parts_in_parenthesis = parts[i + 1:k]
            parts_in_parenthesis.insert(0, partI)
            parts_in_parenthesis.append(partK)
            new_parts.append(evaluate(parts_in_parenthesis))
            i = k + 1
        else:
            new_parts.append(parts[i])
            i += 1
    total = 0
    current_operation = sum
    for part in new_parts:
        if part == "+":
            current_operation = sum
        elif part == "*":
            current_operation = prod
        else:
            total = current_operation([total, int(part)])
    return total

def evaluate_2(parts):
    new_parts = []
    i = 0
    while i < len(parts):
        opened_parenthesis_count = 0
        closed_parenthesis_count = 0
        if parts[i].startswith("("):
            opened_parenthesis_count += parts[i].count("(")
            k = i + 1
            while closed_parenthesis_count != opened_parenthesis_count:
                k += 1
                opened_parenthesis_count += parts[k].count("(")
                closed_parenthesis_count += parts[k].count(")")
            partI = parts[i][1:]
            partK = parts[k][:-1]
            parts_in_parenthesis = parts[i + 1:k]
            parts_in_parenthesis.insert(0, partI)
            parts_in_parenthesis.append(partK)
            new_parts.append(evaluate_2(parts_in_parenthesis))
            i = k + 1
        else:
            new_parts.append(parts[i])
            i += 1
    new_parts_split = " ".join(map(str, new_parts)).split(" * ")
    new_new_parts = []
    for part in new_parts_split:
        part_split = part.split(" + ")
        new_new_parts.append(sum(map(int,part_split)))
    return prod(new_new_parts)


def part_1(inp):
    return sum(map(lambda x : evaluate(x.split(" ")), inp))


def part_2(inp):
    return sum(map(lambda x : evaluate_2(x.split(" ")), inp))


if __name__ == '__main__':
    inp = read_lines_in_file_as_string_array()
    print(evaluate("1 + 2 * 3 + 4 * 5 + 6".split(" ")))
    print(evaluate("2 * 3 + (4 * 5)".split(" ")))
    print(evaluate("((2 + 4 * 9) * (6 + 9 * 8 + 6) + 6) + 2 + 4 * 2".split(" ")))
    print(evaluate_2("5 * 9 * (7 * 3 * 3 + 9 * 3 + (8 + 6 * 4))".split(" ")))
    print(part_1(inp))
    print(part_2(inp))
