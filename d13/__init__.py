from math import ceil, prod

from common import read_lines_in_file_as_string_array


def part_1(expectation, bus_nums):
    m = [(bus_num, (ceil(expectation / bus_num) * bus_num)) for bus_num in bus_nums]
    minimum = min(x[1] for x in m)
    m_min = next(x for x in m if x[1] == minimum)
    return (m_min[1] - expectation) * m_min[0]


def part_2(busses):
    product = prod(x[0] for x in busses)
    aiei = []
    for bus in busses:
        ni = int(product / bus[0])
        k = 1
        while (k * ni) % bus[0] != 1:
            k += 1
        ei = k * ni
        aiei.append(bus[1] * ei)
    return sum(aiei) % product


def build(input):
    return int(input[0]), list(map(int, filter(lambda x: x != "x", input[1].split(","))))


def build_2(inp):
    busses = [x if x == "x" else int(x) for x in inp[1].split(",")]
    return [(busses[i], busses[i]-i) for i in range(len(busses)) if busses[i] != "x"]


if __name__ == '__main__':
    inp = read_lines_in_file_as_string_array()
    (expectation, bus_nums) = build(inp)
    print(part_1(expectation, bus_nums))
    print(part_2(build_2(inp)))
