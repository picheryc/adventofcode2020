from common import read_lines_in_file_as_string_array


def build(inp):
    instructions = []
    for line in inp:
        i = 0
        directions = []
        while i < len(line):
            if line[i] in ["e", "w"]:
                directions.append(line[i])
                i += 1
            else:
                directions.append(line[i:i + 2])
                i += 2
        instructions.append(directions)
    return instructions


def move(position, direction):
    if direction == "e":
        return position[0] + 2, position[1]
    elif direction == "w":
        return position[0] - 2, position[1]
    elif direction == "ne":
        return position[0] + 1, position[1] + 1
    elif direction == "nw":
        return position[0] - 1, position[1] + 1
    elif direction == "sw":
        return position[0] - 1, position[1] - 1
    elif direction == "se":
        return position[0] + 1, position[1] - 1


#  0 0 0
# 0 0 0 0
#  0 0 0

def part_1(instructions):
    grid = build_grid(instructions)
    return sum(grid.values())


adjacent_relative = [[-2, 0], [2, 0], [-1, -1], [-1, 1], [1, -1], [1, 1]]


def should_be_flipped(grid, position):
    number_of_adjacent_black = 0
    adjacents = list(map(lambda adjacent: (position[0] + adjacent[0], position[1] + adjacent[1]), adjacent_relative))
    for adjacent in adjacents:
        if adjacent in grid.keys():
            if grid[adjacent] == 1:
                number_of_adjacent_black += 1
    if position in grid.keys():
        if grid[position] == 1:
            if number_of_adjacent_black == 0 or number_of_adjacent_black > 2:
                return True
        else:
            if number_of_adjacent_black == 2:
                return True
    else:
        if number_of_adjacent_black == 2:
            return True
    return False


def part_2(instructions):
    grid = build_grid(instructions)
    for i in range(100):
        min_x = min([x[0] for x in grid.keys()])
        max_x = max([x[0] for x in grid.keys()])
        min_y = min([x[1] for x in grid.keys()])
        max_y = max([x[1] for x in grid.keys()])
        to_flip = []
        for y in range(min_y - 1, max_y + 2):
            for x in range(min_x - 2, max_x + 3):
                if x % 2 != y % 2:
                    continue
                if should_be_flipped(grid, (x, y)):
                    to_flip.append((x, y))
        for flip in to_flip:
            if flip in grid.keys():
                grid[flip] = 1 - grid[flip]
            else:
                grid[flip] = 1
    return sum(grid.values())


def build_grid(instructions):
    grid = {}
    for instruction in instructions:
        position = (0, 0)
        for direction in instruction:
            position = move(position, direction)
        if position in grid.keys():
            grid[position] = 1 - grid[position]
        else:
            grid[position] = 1
    return grid


if __name__ == '__main__':
    inp = read_lines_in_file_as_string_array()
    test = read_lines_in_file_as_string_array("test_input.txt")
    print(part_1(build(inp)))
    print(part_2(build(inp)))
