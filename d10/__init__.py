from numpy import sort

from common import read_lines_in_file_as_int


def part_1(adapters):
    index = 0
    ones = 0
    threes = 0
    while index < len(adapters) - 1:
        if adapters[index + 1] - adapters[index] == 1:
            ones += 1
        else:
            threes += 1
        index += 1
    return ones * (threes + 1)


def part_2(adapters):
    counts = [1 if x in adapters else 0 for x in range(max(adapters) + 3)]
    for adapter in adapters:
        counts[adapter] = sum(counts[i] for i in range(adapter - 3, adapter))
    return max(counts)


def to_adapters(inp):
    adapters = list(sort(inp))
    adapters.insert(0, 0)
    return adapters


if __name__ == '__main__':
    inp = read_lines_in_file_as_int()
    inp2 = read_lines_in_file_as_int("test_input.txt")
    inp3 = read_lines_in_file_as_int("test_input_2.txt")
    print(part_1(to_adapters(inp)))
    print(part_2(to_adapters(inp)))
    print(part_2(to_adapters(inp2)))
    print(part_2(to_adapters(inp3)))
