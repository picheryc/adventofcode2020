from common import read_lines_in_file_as_string_array
import sys

sys.setrecursionlimit(10000)


def build(inp):
    player = 0
    player_1 = []
    player_2 = []
    for line in inp:
        if line.startswith("Player"):
            player += 1
        elif line == "":
            continue
        else:
            if player == 1:
                player_1.append(int(line))
            else:
                player_2.append(int(line))
    return player_1, player_2


def part_1(player_1, player_2):
    while len(player_1) != 0 and len(player_2) != 0:
        player_1_first_card = player_1[0]
        player_2_first_card = player_2[0]
        if player_1_first_card > player_2_first_card:
            player_1 = player_1[1:]
            player_2 = player_2[1:]
            player_1.append(player_1_first_card)
            player_1.append(player_2_first_card)
        else:
            player_2 = player_2[1:]
            player_1 = player_1[1:]
            player_2.append(player_2_first_card)
            player_2.append(player_1_first_card)
    count = 0
    player = player_1 if len(player_1) != 0 else player_2
    for i in range(len(player)):
        count += player[i] * (len(player) - i)
    return count


def play(player_1, player_2, previous_rounds):
    if (player_1) in previous_rounds:
        return 1
    previous_rounds.append(([x for x in player_1]))
    if len(player_1) == 0 or len(player_2) == 0:
        return 1 if len(player_1) != 0 else 2
    player_1_first_card = player_1.pop(0)
    player_2_first_card = player_2.pop(0)
    if len(player_1) >= player_1_first_card and len(player_2) >= player_2_first_card:
        winner = play([x for x in player_1[:player_1_first_card]], [x for x in player_2[:player_2_first_card]], [])
        if winner == 1:
            player_1.append(player_1_first_card)
            player_1.append(player_2_first_card)
        else:
            player_2.append(player_2_first_card)
            player_2.append(player_1_first_card)
    else:
        if player_1_first_card > player_2_first_card:
            player_1.append(player_1_first_card)
            player_1.append(player_2_first_card)
        else:
            player_2.append(player_2_first_card)
            player_2.append(player_1_first_card)
    return play(player_1, player_2, previous_rounds)


def part_2(player_1, player_2):
    play(player_1, player_2, [])
    count = 0
    player = player_1 if len(player_1) != 0 else player_2
    for i in range(len(player)):
        count += player[i] * (len(player) - i)
    return count


if __name__ == '__main__':
    inp = read_lines_in_file_as_string_array()
    test = read_lines_in_file_as_string_array("test_input.txt")
    (player_1, player_2) = build(inp)
    (player_1_test, player_2_test) = build(test)
    print(part_1(player_1, player_2))
    print(part_2(player_1, player_2))
