from common import read_lines_in_file_as_int


def part_1(inp):
    for i in inp:
        for j in inp:
            if i + j == 2020:
                return i * j


def part_2_short(inp):
    return [(i * j * k) for i in inp for j in inp[:-1] for k in inp[:-2] if i + j + k == 2020][0]


def part_2(inp):
    for i in inp:
        for j in inp:
            for k in inp:
                if i + j + k == 2020:
                    return i * j * k


if __name__ == '__main__':
    print(part_1(read_lines_in_file_as_int()))
    print(part_2(read_lines_in_file_as_int()))
    print(part_2_short(read_lines_in_file_as_int()))
