from common import read_lines_in_file_as_string_array


def get_all_ingredients(foods):
    all_ingredients = []
    for food in foods:
        all_ingredients += food[0]
    return set(all_ingredients)


def part_1(foods):
    all_ingredients = get_all_ingredients(foods)
    ingredient_possible_allergens = get_alergen_list(foods, all_ingredients)
    not_suspicious = list(filter(lambda x: len(ingredient_possible_allergens[x]) == 0, all_ingredients))
    count = 0
    for not_sus in not_suspicious:
        for food in foods:
            count += food[0].count(not_sus)
    return count


def get_alergen_list(foods, all_ingredients):
    ingredient_possible_allergens = {}
    for ingredient in all_ingredients:
        possible_allergens = []
        for i in range(len(foods)):
            food = foods[i]
            if ingredient in food[0]:
                possible_allergens += food[1]
        ingredient_possible_allergens[ingredient] = list(set(possible_allergens))
    for food in foods:
        for ingredient in all_ingredients:
            if ingredient not in food[0]:
                possible_allergens = ingredient_possible_allergens[ingredient]
                ingredient_possible_allergens[ingredient] = list(filter(lambda x: x not in food[1], possible_allergens))
    return ingredient_possible_allergens


def part_2(foods):
    all_ingredients = list(get_all_ingredients(foods))
    ingredient_possible_allergens = get_alergen_list(foods, all_ingredients)
    allergen_for_ingredient = {}
    i = 0
    while i < len(all_ingredients):
        ingredient = all_ingredients[i]
        if len(ingredient_possible_allergens[ingredient]) == 1:
            allergen = ingredient_possible_allergens[ingredient][0]
            allergen_for_ingredient[allergen] = ingredient
            for val in ingredient_possible_allergens.values():
                if allergen in val:
                    val.remove(allergen)
            i = -1
        i += 1
    allergens = [x for x in allergen_for_ingredient.keys()]
    allergens.sort()
    print(allergens)
    ingredients = [allergen_for_ingredient[x] for x in allergens]
    return ",".join(ingredients)


def build(inp):
    foods = []
    for line in inp:
        split = line.split(" (contains ")
        ingredients = split[0].split(" ")
        alergens = split[1][:-1].split(", ")
        foods.append((ingredients, alergens))
    return foods


if __name__ == '__main__':
    inp = read_lines_in_file_as_string_array()
    print(build(inp))
    print(part_1(build(inp)))
    print(part_2(build(inp)))
