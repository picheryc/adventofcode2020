import unittest

from common import read_lines_in_file_as_int
from d9 import has_two_that_sum


class TestDay(unittest.TestCase):


    def test_dum(self):
        input = read_lines_in_file_as_int("test_input.txt")
        res = 0
        for i in range (5, len(input)):
            last_n = input[i-5: i]
            if not has_two_that_sum(last_n, input[i]):
                res = input[i]
        self.assertEqual(res, 127)



if __name__ == '__main__':
    unittest.main()
