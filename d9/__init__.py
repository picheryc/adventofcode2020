from common import read_lines_in_file_as_int


def has_two_that_sum(last_n, val):
    for i in range(len(last_n) - 1):
        for j in range(i + 1, len(last_n)):
            if last_n[i] + last_n[j] == val:
                return True
    return False


def part_1(inp):
    return next(inp[i] for i in range(25, len(inp)) if not has_two_that_sum(inp[i - 25: i], inp[i]))


def part_2(inp, invalid):
    for i in range(0, len(inp) - 1):
        for j in range(i + 2, len(inp)):
            s = sum(inp[i: j])
            if s > invalid:
                break
            if s == invalid:
                return min(inp[i: j]) + max(inp[i: j])
    return 0


if __name__ == '__main__':
    inp = read_lines_in_file_as_int()
    invalid = part_1(inp)
    print(invalid)
    print(part_2(inp, invalid))
