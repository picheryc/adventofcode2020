import unittest

from d5 import get_row, get_column, get_unique_id, get_all_seats, find_missing_seat_id


class TestDay(unittest.TestCase):

    def test_get_row(self):
        row_code = "FBFBBFF"
        self.assertEqual(get_row(row_code), 44)

    def test_get_column(self):
        column_code = "RLR"
        self.assertEqual(get_column(column_code), 5)

    def test_get_unique_id(self):
        boarding_pass_code = "FBFBBFFRLR"
        self.assertEqual(get_unique_id(boarding_pass_code), 357)

    def test_get_all_seats(self):
        inp = ["FBFBBFFRLR", "FBFBBFFRLR"]
        self.assertEqual(get_all_seats(inp), [357, 357])

    def test_find_missing_seat_id(self):
        seat_ids = [1, 2, 3, 5, 6, 7]
        self.assertEqual(find_missing_seat_id(seat_ids), 4)


if __name__ == '__main__':
    unittest.main()
