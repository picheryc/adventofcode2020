from functools import reduce

from common import read_lines_in_file_as_string_array


def compute_half(interval):
    return int((sum(interval) + 1) / 2)


def halven_interval(half_code):
    return lambda interval: [interval[0], compute_half(interval)] if half_code in ["F", "L"] \
        else [compute_half(interval), interval[1]]


def pipe(array_of_functions):
    return lambda interval: reduce(lambda f, g: g(f), array_of_functions, interval)


def halving_function(halving_code):
    return lambda interval: pipe([halven_interval(half_code) for half_code in halving_code])(interval)


def get_row(row_code):
    return halving_function(row_code)([0, 127])[0]


def get_column(column_code):
    return halving_function(column_code)([0, 8])[0]


def get_unique_id(boarding_pass_code):
    row_halving_code = boarding_pass_code[:-3]
    column_halving_code = boarding_pass_code[-3:]
    return get_row(row_halving_code) * 8 + get_column(column_halving_code)


def get_all_seats(inp):
    return [get_unique_id(boarding_pass_code) for boarding_pass_code in inp]


def find_missing_seat_id(seat_ids):
    return next(id for id in range(128 * 8) if id not in seat_ids and id - 1 in seat_ids and id + 1 in seat_ids)


def part_1(inp):
    return max([get_unique_id(boarding_pass_code) for boarding_pass_code in inp])


def part_2(inp):
    return find_missing_seat_id(get_all_seats(inp))


if __name__ == '__main__':
    input_as_string_array = read_lines_in_file_as_string_array()
    print(part_1(input_as_string_array))
    print(part_2(input_as_string_array))
