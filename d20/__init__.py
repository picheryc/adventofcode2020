from math import prod, sqrt

from numpy import rot90

from common import read_lines_in_file_as_string_array


def sides(tile):
    x_0 = [x[0] for x in tile]
    x_max = [x[-1] for x in tile]
    return [
        tile[0],
        tile[-1],
        tile[0][::-1],
        tile[-1][::-1],
        x_0,
        x_max,
        x_0[::-1],
        x_max[::-1],
    ]


def find_match(tile, tiles):
    matches = []
    tile_sides = sides(tile)
    for id_to_match in tiles.keys():
        to_match = tiles[id_to_match]
        if to_match != tile:
            sides_to_match = sides(to_match)
            for side in sides_to_match:
                if side in tile_sides:
                    matches.append(id_to_match)
                    break
    return matches


def find_order(tiles):
    all_matches = {}
    for tile_id in tiles.keys():
        tile = tiles[tile_id]
        all_matches[tile_id] = find_match(tile, tiles)
    corner_tiles = list(filter(lambda tile_id: len(all_matches[tile_id]) == 2, tiles.keys()))
    border_tiles = list(filter(lambda tile_id: len(all_matches[tile_id]) == 3, tiles.keys()))
    center_tiles = list(filter(lambda tile_id: len(all_matches[tile_id]) == 4, tiles.keys()))
    perimeter = len(corner_tiles) + len(border_tiles) + 4
    size = int(perimeter / 4)
    current_tile = all_matches[corner_tiles[0]][0]
    used_tiles = [corner_tiles[0], current_tile]
    x = 2
    y = 0
    while len(used_tiles) != len(tiles):
        if y == 0:
            if x == size - 1:
                list_next = list(
                    filter(lambda tile: tile not in used_tiles and tile in corner_tiles, all_matches[current_tile]))
                current_tile = list_next[0]
                used_tiles.append(current_tile)
                x = 0
                y = 1
            else:
                list_next = list(
                    filter(lambda tile: tile not in used_tiles and tile in border_tiles, all_matches[current_tile]))
                current_tile = list_next[0]
                used_tiles.append(current_tile)
                x += 1
        elif y < size - 1:
            if x == 0:
                current_tile = used_tiles[(y - 1) * size]
                list_next = list(
                    filter(lambda tile: tile not in used_tiles and tile in border_tiles, all_matches[current_tile]))
                current_tile = list_next[0]
                used_tiles.append(current_tile)
                x += 1
            elif x != size - 1:
                upper_tile = used_tiles[(y - 1) * size + x]
                list_next = list(
                    filter(lambda tile: tile not in used_tiles and tile in center_tiles and tile in all_matches[
                        upper_tile], all_matches[current_tile]))
                current_tile = list_next[0]
                used_tiles.append(current_tile)
                x += 1
            else:
                upper_tile = used_tiles[(y - 1) * size + x]
                list_next = list(
                    filter(lambda tile: tile not in used_tiles and tile in border_tiles and tile in all_matches[
                        upper_tile], all_matches[current_tile]))
                current_tile = list_next[0]
                used_tiles.append(current_tile)
                x = 0
                y += 1
        else:
            current_tile = used_tiles[(y - 1) * size + x]
            list_next = list(filter(lambda tile: tile not in used_tiles, all_matches[current_tile]))
            current_tile = list_next[0]
            used_tiles.append(current_tile)
            x += 1
    return used_tiles


def part_1(tiles):
    corner_tiles = list(filter(lambda tile_id: len(find_match(tiles[tile_id], tiles)) == 2, tiles.keys()))
    return prod(corner_tiles)


def rotate(tile):
    return rot90(tile)


def find_arrangement(order, tiles):
    size = int(sqrt(len(order)))
    arrangement = []
    first_corner = order[0]
    first_border = order[1]
    first_border_sides = sides(tiles[first_border])
    tile = tiles[first_corner]
    k = 0
    while [x[-1] for x in tile] not in first_border_sides:
        if k == 3:
            tile = tile[::-1]
        else:
            tile = rotate(tile)
        k += 1
    arrangement.append(tile)
    for i in range(1, len(order)):
        tile = tiles[order[i]]
        if i < size:
            k = 0
            right_side_of_last_tile = [x[-1] for x in arrangement[-1]]
            while [x[0] for x in tile] != right_side_of_last_tile:
                if k == 3:
                    tile = tile[::-1]
                else:
                    tile = rotate(tile)
                k += 1
            arrangement.append(tile)
        else:
            k = 0
            bottom_of_tile_above = [x for x in arrangement[i - size][-1]]
            while [x for x in tile[0]] != bottom_of_tile_above:
                if k == 3:
                    tile = tile[::-1]
                else:
                    tile = rotate(tile)
                k += 1
            arrangement.append(tile)
    return arrangement


def create_full_image(arrangement):
    size = int(sqrt(len(arrangement)))
    grid = []
    for tile in arrangement:
        grid.append([[x for x in line[1:-1]] for line in tile[1:-1]])
    full_image = [[] for x in range(size * len(grid[0]))]
    for Y in range(size):
        for X in range(size):
            tile = grid[Y * size + X]
            for y in range(len(tile)):
                line = tile[y]
                full_image[Y * len(tile) + y] += line
    return full_image


def compute_coords(nessie):
    coords = []
    for y in range(len(nessie)):
        for x in range(len(nessie[0])):
            if nessie[y][x] == "#":
                coords.append((x, y))
    return coords


def find_sea_monster(full_image, nessie_relative_coords):
    coords = []
    for y in range(len(full_image) - 1):
        for x in range(len(full_image[0]) - 18):
            found = True
            for nessie_relative_coord in nessie_relative_coords:
                if full_image[y + nessie_relative_coord[1]][x + nessie_relative_coord[0]] != "#":
                    found = False
                    break
            if found:
                coords += [(x + coord[0], y + coord[1]) for coord in nessie_relative_coords]
    return coords


def part_2(tiles):
    order = find_order(tiles)
    arrangement = find_arrangement(order, tiles)
    full_image = create_full_image(arrangement)
    nessie_relative_coords = compute_coords(["                  # ", "#    ##    ##    ###", " #  #  #  #  #  #   "])
    k = 0
    coords_sea_monster = find_sea_monster(full_image, nessie_relative_coords)
    while len(coords_sea_monster) == 0:
        if k == 3:
            full_image = full_image[::-1]
        else:
            full_image = rotate(full_image)
        coords_sea_monster = find_sea_monster(full_image, nessie_relative_coords)
        k += 1
    count = 0
    for y in range(len(full_image)):
        for x in range(len(full_image[0])):
            if (x, y) not in coords_sea_monster and full_image[y][x] == '#':
                count += 1
    return count


def build(inp):
    tiles = {}
    tile = []
    for i in range(len(inp)):
        line = inp[i]
        if line.startswith("Tile"):
            tile_id = int(line.split(" ")[1][:-1])
            tile = []
            tiles[tile_id] = tile
        elif line == "":
            continue
        else:
            tile.append([x for x in line])
    return tiles


if __name__ == '__main__':
    inp = read_lines_in_file_as_string_array()
    tiles = build(inp)
    print(part_1(tiles))
    print(part_2(tiles))
