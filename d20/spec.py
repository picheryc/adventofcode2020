import unittest

from common import read_lines_in_file_as_string_array
from d20 import build, find_match, find_order


class TestDay(unittest.TestCase):

    def test_match(self):
        inp = read_lines_in_file_as_string_array("test_input.txt")
        tiles = build(inp)
        self.assertEqual([2311, 2729], find_match(tiles[1951], tiles))
        self.assertEqual([2311, 2473], find_match(tiles[3079], tiles))

    def test_find_order(self):
        inp = read_lines_in_file_as_string_array("test_input.txt")
        tiles = build(inp)
        order = [[1951, 2311, 3079], [2729, 1427, 2473], [2971, 1489, 1171]]
        self.assertEqual(order, find_order(tiles))


if __name__ == '__main__':
    unittest.main()
