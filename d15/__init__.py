def part_1(inp):
    while len(inp) <= 2019:
        already_spoken = inp[-1] in inp[:-1]
        if not already_spoken:
            inp.append(0)
        else:
            reversed_inp = inp[:-1][::-1]
            inp.append(reversed_inp.index(inp[-1]) + 1)
    return inp[-1]


def part_2(inp):
    positions = {inp[i]: i + 1 for i in range(len(inp) - 1)}
    last = inp[-1]
    i = len(inp)
    ret = []
    while i <= 30000000 - 1:
        if last in positions:
            position_of_last = positions[last]
            positions[last] = i
            last = i - position_of_last
        else:
            positions[last] = i
            last = 0
        i += 1
        ret.append(last)
    return last


def build(input):
    return input


if __name__ == '__main__':
    inp = [0, 1, 5, 10, 3, 12, 19]
    print(part_1([x for x in inp]))
    print(part_2([x for x in inp]))
