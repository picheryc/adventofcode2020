def get_loop_size(pub):
    val = 1
    subject_number = 7
    index = 0
    mod = 20201227
    while val != pub:
        val = (val * subject_number) % mod
        index += 1
    return index


def transform(subject_number, loop_size):
    val = 1
    mod = 20201227
    for i in range(loop_size):
        val = (val * subject_number) % mod
    return val


def part_1(card_pub, door_pub):
    loop_size_card = get_loop_size(card_pub)
    loop_size_door = get_loop_size(door_pub)
    secret = transform(door_pub, loop_size_card)
    secret2 = transform(card_pub, loop_size_door)
    return secret, secret2


def part_2(card_pub, door_pub):
    return "lol y'en a pas"


if __name__ == '__main__':
    card_pub = 2959251
    door_pub = 4542595
    print(get_loop_size(5764801))
    print(get_loop_size(17807724))
    print(transform(5764801, 11))
    print(part_1(card_pub, door_pub))
    print(part_2(card_pub, door_pub))
