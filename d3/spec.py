import unittest

from common import read_lines_in_file_as_string_array
from d3 import part_1, part_2

test_input = read_lines_in_file_as_string_array("test_input.txt")


class TestDay(unittest.TestCase):

    def test_part_1(self):
        self.assertEqual(part_1(test_input), 7)

    def test_part_2(self):
        self.assertEqual(part_2(test_input), 336)


if __name__ == '__main__':
    unittest.main()
