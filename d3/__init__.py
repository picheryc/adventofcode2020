from math import prod
from numpy import stack

from common import read_lines_in_file_as_string_array


def is_tree_encountered(inp, x, y):
    return inp[y][x] == "#"


def number_of_trees_encountered(inp, d_x, d_y):
    positions_y = list(range(0, len(inp), d_y))
    positions_x = [(d_x * x) % len(inp[0]) for x in range(len(positions_y))]
    return sum([is_tree_encountered(inp, x, y) for x, y in stack((positions_x, positions_y), 1)])


def part_1(inp):
    return number_of_trees_encountered(inp, 3, 1)


def part_2(inp):
    slopes = [[1, 1], [3, 1], [5, 1], [7, 1], [1, 2]]
    return prod([number_of_trees_encountered(inp, d_x, d_y) for [d_x, d_y] in slopes])


if __name__ == '__main__':
    input_as_string_array = read_lines_in_file_as_string_array()
    print(part_1(input_as_string_array))
    print(part_2(input_as_string_array))
