from common import read_lines_in_file_as_string_array


def add_space(grid):
    len_y = len(grid[0])
    len_x = len(grid[0][0])
    grid.insert(0, ["." * len_x] * len_y)
    grid.append(["." * len_x] * len_y)
    for y in grid:
        y.insert(0, "." * len_x)
        y.append("." * len_x)
    for y in grid:
        for i in range(len(y)):
            y[i] = "." + y[i] + "."

def count_neighbours(x, y, z, grid):
    count = 0
    for dx in range(-1, 2):
        for dy in range(-1, 2):
            for dz in range(-1, 2):
                if dx == 0 and dy == 0 and dz == 0:
                    continue
                if x + dx < 0 or y + dy < 0 or z + dz < 0:
                    continue
                if x + dx == len(grid[0][0]) or y + dy == len(grid[0]) or z + dz == len(grid):
                    continue
                else:
                    if grid[z + dz][y + dy][x + dx] == "#":
                        count += 1
    return count


def count_alive(grid):
    count = 0
    for z in range(len(grid)):
        for y in range(len(grid[0])):
            for x in range(len(grid[0][0])):
                if grid[z][y][x] == "#":
                    count += 1
    return count


def add_space_2(grid):
    len_z = len(grid[0])
    len_y = len(grid[0][0])
    len_x = len(grid[0][0][0])
    grid.insert(0, [["." * len_x for _ in range(len_y)] for __ in range(len_z)])
    grid.append([["." * len_x for _ in range(len_y)] for __ in range(len_z)])
    for z in grid:
        z.insert(0, ["." * len_x for _ in range(len_y)])
        z.append(["." * len_x for _ in range(len_y)])
    for z in grid:
        for y in z:
            y.insert(0, "." * len_x)
            y.append("." * len_x)
    for z in grid:
        for y in z:
            for i in range(len(y)):
                y[i] = "." + y[i] + "."
def count_neighbours_2(w, z, y, x, grid):
    count = 0
    for dx in range(-1, 2):
        if x + dx < 0 or x + dx == len(grid[0][0][0]):
            continue
        for dy in range(-1, 2):
            if y + dy < 0 or y + dy == len(grid[0][0]):
                continue
            for dz in range(-1, 2):
                if z + dz < 0 or z + dz == len(grid[0]):
                    continue
                for dw in range(-1, 2):
                    if w + dw < 0 or w + dw == len(grid):
                        continue
                    if dx == 0 and dy == 0 and dz == 0 and dw == 0:
                        continue
                    if grid[w + dw][z + dz][y + dy][x + dx] == "#":
                        count += 1
    return count


def count_alive_2(grid):
    count = 0
    for w in range(len(grid)):
        for z in range(len(grid[0])):
            for y in range(len(grid[0][0])):
                for x in range(len(grid[0][0][0])):
                    if grid[w][z][y][x] == "#":
                        count += 1
    return count


def part_1(grid):
    for cyles in range(6):
        add_space(grid)
        to_change = []
        for z in range(len(grid)):
            for y in range(len(grid[0])):
                for x in range(len(grid[0][0])):
                    count = count_neighbours(x, y, z, grid)
                    if grid[z][y][x] == ".":
                        if count == 3:
                            to_change.append((z, y, x, "#"))
                    else:
                        if count != 3 and count != 2:
                            to_change.append((z, y, x, "."))
        for change in to_change:
            x = grid[change[0]][change[1]]
            grid[change[0]][change[1]] = x[:change[2]] + change[3] + x[change[2] + 1:]
    return count_alive(grid)


def part_2(grid):
    for cycles in range(6):
        add_space_2(grid)
        to_change = []
        for w in range(len(grid)):
            for z in range(len(grid[0])):
                for y in range(len(grid[0][0])):
                    for x in range(len(grid[0][0][0])):
                        count = count_neighbours_2(w, z, y, x, grid)
                        if grid[w][z][y][x] == ".":
                            if count == 3:
                                to_change.append((w, z, y, x, "#"))
                        else:
                            if count != 3 and count != 2:
                                to_change.append((w, z, y, x, "."))
        for change in to_change:
            x = grid[change[0]][change[1]][change[2]]
            grid[change[0]][change[1]][change[2]] = x[:change[3]] + change[4] + x[change[3] + 1:]
    return count_alive_2(grid)


if __name__ == '__main__':
    inp = read_lines_in_file_as_string_array()
    print(part_1([[x for x in inp]]))
    print(part_2([[[x for x in inp]]]))
