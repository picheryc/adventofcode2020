from common import read_lines_in_file_as_string_array

directions = [(1, 0), (0, 1), (-1, 0), (0, -1)]


def compute_direction(direction, value):
    return (direction + int(value / 90)) % 4


def move(instruction, position, direction):
    instruction_key = instruction[0]
    instruction_val = instruction[1]
    if instruction_key == "F":
        position[0] += directions[direction][0] * instruction_val
        position[1] += directions[direction][1] * instruction_val
        return position, direction
    if instruction_key == "N":
        position[1] += instruction_val
        return position, direction
    if instruction_key == "S":
        position[1] -= instruction_val
        return position, direction
    if instruction_key == "E":
        position[0] += instruction_val
        return position, direction
    if instruction_key == "W":
        position[0] -= instruction_val
        return position, direction
    instruction_angle = instruction_val if instruction_key == "L" else -instruction_val
    direction = compute_direction(direction, instruction_angle)
    return position, direction


def part_1(input):
    direction = 0
    position = [0, 0]
    for instruction in input:
        (position, direction) = move(instruction, position, direction)
    return abs(position[0]) + abs(position[1])


def compute_waypoint(waypoint, instruction_angle):
    val = instruction_angle % 360
    x = waypoint[0]
    y = waypoint[1]
    if val == 90:
        return [-y, x]
    elif val == 180:
        return [-x, -y]
    else:
        return [y, -x]


def move_2(instruction, position, waypoint):
    instruction_key = instruction[0]
    instruction_val = instruction[1]
    if instruction_key == "F":
        position[0] += waypoint[0] * instruction_val
        position[1] += waypoint[1] * instruction_val
        return position, waypoint
    if instruction_key == "N":
        waypoint[1] += instruction_val
        return position, waypoint
    if instruction_key == "S":
        waypoint[1] -= instruction_val
        return position, waypoint
    if instruction_key == "E":
        waypoint[0] += instruction_val
        return position, waypoint
    if instruction_key == "W":
        waypoint[0] -= instruction_val
        return position, waypoint
    instruction_angle = instruction_val if instruction_key == "L" else -instruction_val
    waypoint = compute_waypoint(waypoint, instruction_angle)
    return position, waypoint


def part_2(input):
    waypoint = [10, 1]
    position = [0, 0]
    for instruction in input:
        (position, waypoint) = move_2(instruction, position, waypoint)
    return abs(position[0]) + abs(position[1])


def build(input):
    return [(i[0], int(i[1:])) for i in input]


if __name__ == '__main__':
    inp = read_lines_in_file_as_string_array()
    input = build(inp)
    print(part_1(input))
    print(part_2(input))
