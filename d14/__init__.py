import re

from common import read_lines_in_file_as_string_array


def part_1(inp):
    memory = {}
    for (mask, instructions) in inp:
        for instruction in instructions:
            bit_value = ("0000000000000000000000000000000000" + "{0:b}".format(instruction[1]))[-len(mask):]
            masked_value = "".join([bit_value[i] if mask[i] == "X" else mask[i] for i in range(len(bit_value))])
            memory[instruction[0]] = int(masked_value, 2)
    return sum(memory.values())


def part_2(inp):
    memory = {}
    for (mask, instructions) in inp:
        x_pos = [len(mask) - i -1 for i in range(len(mask)) if mask[i] == "X"]
        possible_floating_values = [("0000000000000000000000000000000" + "{0:b}".format(i))[-len(x_pos):] for i in range(2 ** len(x_pos))]
        for instruction in instructions:
            bit_value = ("0000000000000000000000000000000000" + "{0:b}".format(instruction[0]))[-len(mask):]
            floating_memory_address_value = "".join(
                [bit_value[i] if mask[i] == "0" else mask[i] for i in range(len(bit_value))])
            for possible_floating_value in possible_floating_values:
                adress = "".join([
                    floating_memory_address_value[i] if floating_memory_address_value[i] != "X" else
                    possible_floating_value[x_pos.index(len(mask) - i - 1)] for i
                    in range(len(floating_memory_address_value))])
                memory[int(adress, 2)] = instruction[1]
    return sum(memory.values())


def build(input):
    lines = []
    i = 0
    while True:
        if i == len(input):
            return lines
        mask = input[i].split(" = ")[1]
        i += 1
        instructions = []
        lines.append((mask, instructions))
        while i < len(input) and "mask" not in input[i]:
            line = input[i]
            place_in_memory = int(line.split("] = ")[0].split("mem[")[1])
            value = int(line.split("] = ")[1])
            instructions.append((place_in_memory, value))
            i += 1


if __name__ == '__main__':
    inp = read_lines_in_file_as_string_array()
    print(part_1(build(inp)))
    print(part_2(build(inp)))
