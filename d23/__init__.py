def play(cups, index):
    current_cup = cups[index]
    while cups.index(current_cup) != 0:
        cups = [cups[-1]] + cups[:-1]
    to_move = cups[1:4]
    destination = current_cup - 1
    while destination in to_move or destination <= 0:
        destination = destination - 1
        if destination < 1:
            destination = 9
    cut_cups = [current_cup] + cups[4:]
    destination_index = cut_cups.index(destination)
    cups = cut_cups[:destination_index + 1] + to_move + cut_cups[destination_index + 1:]
    while cups.index(current_cup) != index:
        cups = [cups[-1]] + cups[:-1]
    return cups, (index + 1) % len(cups)


def play_3(cups, value):
    to_move_1 = cups[value]
    to_move_2 = cups[to_move_1]
    to_move_3 = cups[to_move_2]
    destination_value = value - 1
    while destination_value in [to_move_1, to_move_2, to_move_3] or destination_value <= 0:
        destination_value -= 1
        if destination_value <= 0:
            destination_value = 1000000
    destination_next = cups[destination_value]
    to_move_next = cups[to_move_3]
    cups[to_move_3] = destination_next
    cups[destination_value] = to_move_1
    cups[value] = to_move_next
    return to_move_next


def part_1(cups):
    cups, index = play(cups, 0)
    for i in range(99):
        cups, index = play(cups, index)
    while cups.index(1) != 0:
        cups = [cups[-1]] + cups[:-1]
    return "".join([str(x) for x in cups[1:]])


def part_2(cups):
    cups = cups + [x for x in range(10, 1000001)]
    cups_with_next = ["" for x in range(1000001)]
    for index in range(len(cups) - 1):
        cups_with_next[cups[index]] = cups[index + 1]
    cups_with_next[1000000] = cups[0]

    value = cups[0]
    for i in range(10000000):
        value = play_3(cups_with_next, value)
    return cups_with_next[1] * cups_with_next[cups_with_next[1]]


if __name__ == '__main__':
    test_input = [int(x) for x in "389125467"]
    input = [int(x) for x in "653427918"]
    print(part_1(input))
    print(part_2(input))
