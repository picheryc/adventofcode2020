from common import read_lines_in_file_as_int, read_lines_in_file_as_string_array, read_first_line_in_file_as_text

def prepare(inp):
    pass

def part_1(inp):
    return inp


def part_2(inp):
    return inp


if __name__ == '__main__':
    print(part_1(read_lines_in_file_as_int()))
    print(part_2(read_lines_in_file_as_string_array()))
    print(part_1(read_first_line_in_file_as_text()))
