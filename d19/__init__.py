from common import read_lines_in_file_as_string_array


def compute_len(rule, rules):
    if rule[0] == "a" or rule[0] == "b":
        return 1
    leng = 0
    for sub_rule in rule[0]:
        leng += compute_len(rules[sub_rule], rules)
    return leng


def build(inp):
    step = 0
    i = 0
    rules = {}
    to_match = []
    pos_a = 0
    pos_b = 0
    while i < len(inp):
        line = inp[i]
        if line == "":
            step = 1
        elif step == 0:
            split = line.split(": ")
            number = int(split[0])
            if split[1].startswith("\""):
                a_or_b = split[1][1:-1]
                rules[number] = a_or_b
                if a_or_b == "a":
                    pos_a = number
                else:
                    pos_b = number
            else:
                raw_parts = split[1].split(" | ")
                rules[number] = [[int(x) for x in y.split(" ")] for y in raw_parts]
        else:
            to_match.append(line)
        i += 1
    return rules, to_match, pos_a, pos_b


def is_valid(word, rules, rule_id):
    sub_rules = rules[rule_id]
    if sub_rules == "a":
        return word.startswith("a"), word[1:]
    if rules[rule_id] == "b":
        return word.startswith("b"), word[1:]
    for sub_rule_ids in sub_rules:
        local_word = word
        valid = True
        for sub_rule_id in sub_rule_ids:
            (valid, local_word) = is_valid(local_word, rules, sub_rule_id)
            if not valid:
                break
        if valid:
            return valid, local_word
    return False, word


def part_1(rules, to_validate):
    count = 0
    for word in to_validate:
        (valid, rest) = is_valid(word, rules, 0)
        if valid and len(rest) == 0:
            count += 1
    return count


# 248 - 387
# 283, 352 and 300 false 401
def part_2(rules, to_validate):
    count = 0
    valid_words = []
    for word in to_validate:
        rest = word
        while True:
            (valid, rest) = is_valid(rest, rules, 42)
            if not valid or len(rest) == 0:
                break
            for i in range(1, 10):
                rule_11 = []
                for j in range(i):
                    rule_11.insert(0,42)
                    rule_11.append(31)
                rules[11] = [rule_11]
                (valid, rest_for_11) = is_valid(rest, rules, 11)
                if valid and len(rest_for_11) == 0:
                    break
            if valid and len(rest_for_11) == 0:
                valid_words.append(word)
                count += 1
                break
    return count, valid_words

            # if len(rest_42) == 0:
            #     valid_31 = False
            #     break
            # (valid_42, rest_42) = is_valid(rest_42, rules, 42)
            # if not valid_42:
            #     break
            # if len(rest_42) == 0:
            #     break
            # for i in range(1, 8):
            #     rest_42_11 = rest_42
            #     for j in range(i):
            #         if len(rest_42_11) == 0 or not valid_42_11:
            #             break
            #         (valid_42_11, rest_42_11) = is_valid(rest_42_11, rules, 42)
            #     rest_31 = rest_42_11
            #     if len(rest_42_11) == 0:
            #         break
            #     for j in range(i):
            #         (valid_31, rest_31) = is_valid(rest_31, rules, 31)
            #     if len(rest_31) == 0:
            #         break
            # if valid_31 and len(rest_31) == 0:
            #     valid_words.append(word)
            #     count += 1
            #     break
    return count, valid_words


if __name__ == '__main__':
    inp = read_lines_in_file_as_string_array()
    (rules, to_validate, pos_a, pos_b) = build(inp)
    print(part_1(rules, to_validate))
    print(part_2(rules, to_validate))
