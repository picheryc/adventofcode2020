import unittest

from common import read_lines_in_file_as_string_array
from d19 import part_2, build


class TestDay(unittest.TestCase):

    def test_example(self):
        expected_valid = ["bbabbbbaabaabba",
                          "babbbbaabbbbbabbbbbbaabaaabaaa",
                          "aaabbbbbbaaaabaababaabababbabaaabbababababaaa",
                          "bbbbbbbaaaabbbbaaabbabaaa",
                          "bbbababbbbaaaaaaaabbababaaababaabab",
                          "ababaaaaaabaaab",
                          "ababaaaaabbbaba",
                          "baabbaaaabbaaaababbaababb",
                          "abbbbabbbbaaaababbbbbbaaaababb",
                          "aaaaabbaabaaaaababaa",
                          "aaaabbaabbaaaaaaabbbabbbaaabbaabaaa",
                          "aabbbbbaabbbaaaaaabbbbbababaaaaabbaaabba"]
        inp = read_lines_in_file_as_string_array("test_input.txt")
        (rules, to_validate, pos_a, pos_b) = build(inp)
        self.assertEqual(part_2(rules, to_validate)[1], expected_valid)


if __name__ == '__main__':
    unittest.main()
