import unittest

from d2 import part_1


class TestDay(unittest.TestCase):

    def test_1_3_a(self):
        self.assertEqual(part_1(["1-3 a: abcde"]), 1)

    def test_1_3_b(self):
        self.assertEqual(part_1(["1-3 b: cdefg"]), 0)

    def test_2_9_c(self):
        self.assertEqual(part_1(["2-9 c: ccccccccc"]), 1)


if __name__ == '__main__':
    unittest.main()
