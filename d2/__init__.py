from common import read_lines_in_file_as_string_array


def prepare(input):
    policy = input.split(":")[0]
    password = input.split(":")[1][1:]
    splitted_policy = policy.split(" ")
    significant_char = splitted_policy[1]
    lower_index = int(splitted_policy[0].split("-")[0])
    upper_index = int(policy.split(" ")[0].split("-")[1])
    return lower_index, upper_index, significant_char, password


def part_1(inp):
    solve = []
    for i in inp:
        (lower_index, upper_index, significant_char, password) = prepare(i)
        count = password.count(significant_char)
        solve.append(count in range(lower_index, upper_index + 1))
    return sum(solve)


def part_2(inp):
    solve = []
    for i in inp:
        (lower_index, upper_index, significant_char, password) = prepare(i)
        matchLower = password[lower_index - 1] == significant_char
        matchUpper = password[upper_index - 1] == significant_char
        solve.append(matchLower ^ matchUpper)
    return sum(solve)


def part_1_short(inp):
    return sum(
        [password.count(significant_char) in range(lower_index, upper_index + 1) for
         lower_index, upper_index, significant_char, password in map(prepare, inp)])


def part_2_short(inp):
    return sum(
        [(password[lower_index - 1] == significant_char) ^ (password[upper_index - 1] == significant_char) for
         lower_index, upper_index, significant_char, password in
         map(prepare, inp)])


if __name__ == '__main__':
    input_as_string_array = read_lines_in_file_as_string_array()
    print(part_1(input_as_string_array))
    print(part_2(input_as_string_array))
    print(part_1_short(input_as_string_array))
    print(part_2_short(input_as_string_array))
