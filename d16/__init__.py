from math import prod

from common import read_lines_in_file_as_string_array


def part_1(ranges, ticket, nearby):
    invalid = []
    tickets = [x for x in nearby]
    tickets.append(ticket)
    for tick in tickets:
        for value in tick:
            valid = False
            for range in ranges:
                if (valid):
                    break
                for r in range[1:]:
                    if value in r:
                        valid = True
                        break
            if not valid:
                invalid.append(value)
    return sum(invalid)


def part_2(ranges, ticket, nearby):
    valid_nearby = []
    for tick in nearby:
        if not has_invalid_fields(ranges, tick):
            valid_nearby.append(tick)
    valid_nearby.append(ticket)
    order = find_order(ranges, valid_nearby)
    departure_indexes = [order[x] for x in [y[0] for y in ranges] if "departure" in x]
    return prod(map(lambda index: ticket[index], departure_indexes))


def is_valid_index(index, r, tickets):
    for ticket in tickets:
        if ticket[index] not in r[1] and ticket[index] not in r[2]:
            return False
    return True


def find_next(possible_values):
    field = ""
    position = 0
    for possible_value in possible_values:
        if len(possible_values[possible_value]) == 1:
            field = possible_value
            position = possible_values[possible_value][0]
            break
    for possible_value in possible_values:
        value = possible_values[possible_value]
        if position in value:
            value.remove(position)
    return field, position


def find_order(ranges, tickets):
    possible_values = {r[0]: [] for r in ranges}
    for index in range(len(tickets[0])):
        for r in ranges:
            if is_valid_index(index, r, tickets):
                possible_values[r[0]].append(index)
    order = {}
    while len(order.keys()) != len(tickets[0]):
        field, position = find_next(possible_values)
        order[field] = position
    return order


def has_invalid_fields(ranges, tick):
    for value in tick:
        valid = False
        for range in ranges:
            if (valid):
                break
            for r in range[1:]:
                if value in r:
                    valid = True
                    break
        if not valid:
            return True
    return False


def build(input):
    step = "ranges"
    ranges = []
    ticket = []
    nearby = []
    i = 0
    while i < len(input):
        if step == "ranges":
            if input[i] == "":
                step = "ticket"
                i += 1
            else:
                type = input[i].split(": ")[0]
                range1 = input[i].split(": ")[1].split(" or ")[0].split("-")
                range2 = input[i].split(": ")[1].split(" or ")[1].split("-")
                ranges.append(
                    [type, range(int(range1[0]), int(range1[1]) + 1), range(int(range2[0]), int(range2[1]) + 1)])
        elif step == "ticket":
            ticket = [int(x) for x in input[i].split(",")]
            i += 2
            step = "nearby"
        else:
            nearby.append([int(x) for x in input[i].split(",")])
        i += 1
    return ranges, ticket, nearby


if __name__ == '__main__':
    inp = read_lines_in_file_as_string_array()
    ranges, ticket, nearby = build(inp)
    print(part_1(ranges, ticket, nearby))
    print(part_2(ranges, ticket, nearby))
